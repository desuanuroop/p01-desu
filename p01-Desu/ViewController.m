//
//  ViewController.m
//  p01-Desu
//
//  Created by Anuroop Desu on 1/30/16.
//  Copyright © 2016 Anuroop Desu. All rights reserved.
//

#import "ViewController.h"
#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self->changeButton.backgroundColor = Rgb2UIColor(51, 153, 255);
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)ButtonPress:(id)sender {
    
    self.view.backgroundColor =  Rgb2UIColor(225, 225, 234);
    messageLabel.text = @"This is Anuroop Desu.";

}

-(IBAction)exitPress:(id)sender{
    
    exit(0);
    
}

@end
