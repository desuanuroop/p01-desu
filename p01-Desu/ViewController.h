//
//  ViewController.h
//  p01-Desu
//
//  Created by Anuroop Desu on 1/30/16.
//  Copyright © 2016 Anuroop Desu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    IBOutlet UILabel *messageLabel;
    
    IBOutlet UIButton *exitButton;
    
    IBOutlet UIButton *changeButton;
    
}
-(IBAction)ButtonPress:(id)sender;
-(IBAction)exitPress:(id)sender;
@end

